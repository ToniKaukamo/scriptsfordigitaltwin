using UnityEngine;
using System.Collections.Generic;
using System.IO;
using Newtonsoft.Json; // JsonUtility has limitations

public class fakeJsonGenerator : MonoBehaviour
{
    [System.Serializable]
    public class SensorInfo
    {
        public string Name;
        public string SensorType;
        public float Value;
    }

    [System.Serializable]
    public class HardwareInfo
    {
        public string Name;
        public List<SensorInfo> SensorsInfo;
    }
    public void GenerateAndSaveFakeData()
    {
        List<HardwareInfo> fakeData = GenerateFakeData();
        SaveToJson(fakeData);
    }

    private List<HardwareInfo> GenerateFakeData()
    {
        List<HardwareInfo> fakeData = new List<HardwareInfo>();
        // MOBO
        fakeData.Add(new HardwareInfo
        {
            Name = "ASUS PRIME Z370-P II",
            SensorsInfo = new List<SensorInfo>()
        });

        // CPU
        var cpuSensors = new List<SensorInfo>
        {
            new SensorInfo { Name = "CPU Core #1", SensorType = "Load", Value = GenerateRandomValue() },
            new SensorInfo { Name = "CPU Core #2", SensorType = "Load", Value = GenerateRandomValue() },
            new SensorInfo { Name = "CPU Core #3", SensorType = "Load", Value = GenerateRandomValue() },
            new SensorInfo { Name = "CPU Core #4", SensorType = "Load", Value = GenerateRandomValue() },
            new SensorInfo { Name = "CPU Core #5", SensorType = "Load", Value = GenerateRandomValue() },
            new SensorInfo { Name = "CPU Core #6", SensorType = "Load", Value = GenerateRandomValue() },
            new SensorInfo { Name = "CPU Core #7", SensorType = "Load", Value = GenerateRandomValue() },
            new SensorInfo { Name = "CPU Core #8", SensorType = "Load", Value = GenerateRandomValue() },
            new SensorInfo { Name = "CPU Total", SensorType = "Load", Value = GenerateRandomValue() },
        };

        fakeData.Add(new HardwareInfo
        {
            Name = "Intel Core i7-9700K",
            SensorsInfo = cpuSensors
        });

        // RAM
        var memorySensors = new List<SensorInfo>
        {
            new SensorInfo { Name = "Memory Used", SensorType = "Data", Value = GenerateRandomValue() },
            new SensorInfo { Name = "Memory Available", SensorType = "Data", Value = GenerateRandomValue() },
            new SensorInfo { Name = "Memory", SensorType = "Load", Value = GenerateRandomValue() },
        };

        fakeData.Add(new HardwareInfo
        {
            Name = "Generic Memory",
            SensorsInfo = memorySensors
        });

        // NVIDIA GeForce RTX 2070
        var gpuSensors = new List<SensorInfo>
        {
            new SensorInfo { Name = "GPU Hot Spot", SensorType = "Temperature", Value = GenerateRandomValue() },
            new SensorInfo { Name = "GPU Memory Total", SensorType = "SmallData", Value = 8192},
            new SensorInfo { Name = "GPU Memory Free", SensorType = "SmallData", Value = 6599 },
            new SensorInfo { Name = "GPU Memory Used", SensorType = "SmallData", Value = 1592 },
            new SensorInfo { Name = "GPU Memory", SensorType = "Load", Value = GenerateRandomValue() },
        };

        fakeData.Add(new HardwareInfo
        {
            Name = "NVIDIA GeForce RTX 2070",
            SensorsInfo = gpuSensors
        });
        return fakeData;
    }

    private float GenerateRandomValue()
    {
        float baseValue = Random.Range(1.0f, 99.9f);
        float deviation = Random.Range(-2.0f, 2.0f);

        return Mathf.Clamp(baseValue + deviation, 1.0f, 99.9f);
    }

    private void SaveToJson(List<HardwareInfo> fakeData)
    {
        string jsonData = JsonConvert.SerializeObject(fakeData, Formatting.Indented);
        string filePath = "fake_hardwaredata.json";
        File.WriteAllText(filePath, jsonData);
        Debug.Log("Fake data saved to: " + filePath);
    }
}
