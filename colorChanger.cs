using System.IO;
using TMPro;
using UnityEngine;
using UnityEngine.UIElements;

public class colorChanger : MonoBehaviour
{
    public MeshRenderer meshRenderer;
    public string componentName;
    public float value;
    public string dataName;
    public TMP_Text HUD_componentNameTXT;
    public TMP_Text HUD_sensorDataTXT;
    // Green-yellow-orange-red gradient
    public Gradient gradient;
    private Color evaluatedColor;
    // Attached to all GO that we want to change
    void Start()
    {
        meshRenderer = GetComponent<MeshRenderer>();
    }

    void Update()
    {
        evaluatedColor = this.ColorFromGradient(value/100);
        meshRenderer.material.color = evaluatedColor;
    }
    Color ColorFromGradient (float value)
    {
	    return gradient.Evaluate(value);
    }
    
}