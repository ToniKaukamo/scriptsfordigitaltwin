using System.Collections.Generic;
using System.IO;
using UnityEngine;

[System.Serializable]
public class SensorInfo
{
    public string Name;
    public string SensorType;
    public float Value;
}

[System.Serializable]
public class HardwareInfo
{
    public string Name;
    public List<SensorInfo> SensorsInfo;
}

[System.Serializable]
public class HardwareInfoWrapper
{
    public List<HardwareInfo> Data;
}

public class jsonParser : MonoBehaviour
{
    private int nextUpdate = 0;
    public GameObject colorControl;
    void Update()
    {
        // Call it every 5 seconds
        if(Time.time>=nextUpdate){
            nextUpdate=Mathf.FloorToInt(Time.time)+5;
            parseJson();
        }
    }
    public void parseJson()
    {  
        string jsonFilePath = "hardware_info.json";
        string jsonText = File.ReadAllText(jsonFilePath);
        HardwareInfoWrapper wrapper = JsonUtility.FromJson<HardwareInfoWrapper>("{\"Data\":" + jsonText + "}");
        foreach (HardwareInfo hardwareInfo in wrapper.Data)
        {
            foreach (SensorInfo sensorInfo in hardwareInfo.SensorsInfo)
            {
                if (float.TryParse(sensorInfo.Value.ToString(), out float floatValue))
                {
                    colorControl.GetComponent<colorChangerController>().UpdateColor(hardwareInfo.Name, sensorInfo.Name, floatValue);
                }
            }
        }
    }
}


