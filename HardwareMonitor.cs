﻿// This is the external exe for getting the data
using System.Runtime.InteropServices;
using System.Security.Principal;
using System.Text.Json;
// https://www.nuget.org/packages/LibreHardwareMonitorLib/ needed for the package
using LibreHardwareMonitor.Hardware;


public class HardwareMonitor
{
    public class UpdateVisitor : IVisitor
    {
        public void VisitComputer(IComputer computer)
        {
            computer.Traverse(this);
        }
        public void VisitHardware(IHardware hardware)
        {
            hardware.Update();
            foreach (IHardware subHardware in hardware.SubHardware) subHardware.Accept(this);
        }
        public void VisitSensor(ISensor sensor) { }
        public void VisitParameter(IParameter parameter) { }
    }
    
    public string Monitor()
    {
        Computer computer = new Computer
        {
            IsCpuEnabled = true,
            IsGpuEnabled = true,
            IsMemoryEnabled = true,
            // https://www.nuvoton.com/resource-files/NCT6796D_Datasheet_V0_6.pdf info on the tested motherboard
            IsMotherboardEnabled = true,
            IsControllerEnabled = true,
            IsNetworkEnabled = true,
            IsStorageEnabled = true
        };

        computer.Open();
        computer.Accept(new UpdateVisitor());

        List<HardwareInfo> hardwareInfoList = new List<HardwareInfo>();

        foreach (IHardware hardware in computer.Hardware)
        {
            HardwareInfo hardwareInfo = new HardwareInfo
            {
                Name = hardware.Name,
                SubHardwareInfo = new List<SubHardwareInfo>(),
                SensorsInfo = new List<SensorInfo>()
            };

            foreach (IHardware subhardware in hardware.SubHardware)
            {
                SubHardwareInfo subHardwareInfo = new SubHardwareInfo
                {
                    Name = subhardware.Name,
                    SensorsInfo = new List<SensorInfo>()
                };

                foreach (ISensor sensor in subhardware.Sensors)
                {
                    if(!(sensor.SensorType.ToString() == "Control" ||
                       sensor.SensorType.ToString() == "Clock" ||
                       sensor.SensorType.ToString() == "Power" ||
                       sensor.SensorType.ToString() == "Throughput"||
                       sensor.SensorType.ToString() == "Voltage"))
                    {
                        SensorInfo sensorInfo = new SensorInfo
                        {
                        Name = sensor.Name,
                        SensorType = sensor.SensorType.ToString(),
                        Value = sensor.Value
                        };
                        subHardwareInfo.SensorsInfo.Add(sensorInfo);
                    }       
                }
                hardwareInfo.SubHardwareInfo.Add(subHardwareInfo);
            }

            foreach (ISensor sensor in hardware.Sensors)
            {   
                if(!(sensor.SensorType.ToString() == "Control" ||
                    sensor.SensorType.ToString() == "Clock" ||
                    sensor.SensorType.ToString() == "Power" ||
                    sensor.SensorType.ToString() == "Throughput"||
                    sensor.SensorType.ToString() == "Voltage"))
                {
                    SensorInfo sensorInfo = new SensorInfo
                    {
                        Name = sensor.Name,
                        SensorType = sensor.SensorType.ToString(),
                        Value = sensor.Value
                    };
                    hardwareInfo.SensorsInfo.Add(sensorInfo);
                }
            }
            hardwareInfoList.Add(hardwareInfo);
        }

        computer.Close();

        // Convert the hardware information list to a JSON string
        string json = JsonSerializer.Serialize(hardwareInfoList, new JsonSerializerOptions { WriteIndented = true });
        return json;
    }
}
public class HardwareInfo
{
    public string? Name { get; set; }
    public List<SubHardwareInfo>? SubHardwareInfo { get; set; }
    public List<SensorInfo>? SensorsInfo { get; set; }
}

public class SubHardwareInfo
{
    public string? Name { get; set; }
    public List<SensorInfo>? SensorsInfo { get; set; }
}

public class SensorInfo
{
    public string? Name { get; set; }
    public string? SensorType { get; set; }
    public float? Value { get; set; }
}

public class Program
{
    static void Main()
    {
        StreamReader sr = new StreamReader(Console.OpenStandardInput());
        string filePath = "hardware_info.json";
        // sensor.SensorType.ToString(); if the current user has administrative privileges (Windows-specific)
        if (IsAdministrator())
        {
            HardwareMonitor monitor = new HardwareMonitor();
            string json = monitor.Monitor();
            File.WriteAllText(filePath, json);
        }
        else
        {   
            HardwareMonitor monitor = new HardwareMonitor();
            string json = monitor.Monitor();
            File.WriteAllText(filePath, json);
        }
    }

    static bool IsAdministrator()
    {
        if (RuntimeInformation.IsOSPlatform(OSPlatform.Windows))
        {
            WindowsIdentity identity = WindowsIdentity.GetCurrent();
            WindowsPrincipal principal = new WindowsPrincipal(identity);
            return principal.IsInRole(WindowsBuiltInRole.Administrator);
        }
        return false;
    }
}
