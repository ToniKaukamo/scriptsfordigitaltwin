using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class colorChangerController : MonoBehaviour
{
    public GameObject GPU; // GPU Hot Spot (temp num)
    private colorChanger GPUcolor;
    public GameObject CPU; // CPU Total 1-100% - Admin CPU Package (temp num)
    private colorChanger CPUcolor;
    public GameObject RAM1; // Memory 1-100%
    private colorChanger RAM1color;
    public GameObject RAM2; // Memory 1-100%
    private colorChanger RAM2color;
    public GameObject RAM3; // Memory 1-100%
    private colorChanger RAM3color;
    public GameObject RAM4; // Memory 1-100%
    private colorChanger RAM4color;
    public GameObject SSD; // admin
    private colorChanger SSDcolor;
    public GameObject FAN1; // admin
    private colorChanger FAN1color;
    public GameObject FAN2; // admin
    private colorChanger FAN2color;
    public GameObject FAN3; // admin
    private colorChanger FAN3color;
    public GameObject FAN4; // admin
    private colorChanger FAN4color;

    void Start()
    {
        this.GPUcolor = GPU.GetComponent<colorChanger>();
        this.CPUcolor = CPU.GetComponent<colorChanger>(); 
        this.RAM1color = RAM1.GetComponent<colorChanger>(); 
        this.RAM2color = RAM2.GetComponent<colorChanger>(); 
        this.RAM3color = RAM3.GetComponent<colorChanger>(); 
        this.RAM4color = RAM4.GetComponent<colorChanger>(); 
        this.SSDcolor = SSD.GetComponent<colorChanger>(); 
        this.FAN1color = FAN1.GetComponent<colorChanger>(); 
        this.FAN2color = FAN2.GetComponent<colorChanger>(); 
        this.FAN3color = FAN3.GetComponent<colorChanger>(); 
        this.FAN4color = FAN4.GetComponent<colorChanger>();
    }

    public void UpdateColor(string componentName, string sensorName, float value)
    {
        switch (sensorName)
        {
            case "CPU Total":
                CPUcolor.value = value;
                CPUcolor.dataName = sensorName;
                CPUcolor.componentName = componentName;
                break;
            case "GPU Hot Spot":
                GPUcolor.value = value;
                GPUcolor.dataName = sensorName;
                GPUcolor.componentName = componentName;
                break;
            case "Memory": //ram
                RAM1color.value = value;
                RAM1color.dataName = sensorName;
                RAM1color.componentName = componentName;
                RAM2color.value = value;
                RAM2color.dataName = sensorName;
                RAM2color.componentName = componentName;
                RAM3color.value = value;
                RAM3color.dataName = sensorName;
                RAM3color.componentName = componentName;
                RAM4color.value = value;
                RAM4color.dataName = sensorName;
                RAM4color.componentName = componentName;
                break;
            //Possible with adming rights
            case "Used Space": //ssd
                SSDcolor.value = value;
                SSDcolor.dataName = sensorName;
                SSDcolor.componentName = componentName;
                break;
            case "Fan #1":
                FAN1color.value = value/100;
                FAN1color.dataName = sensorName;
                FAN1color.componentName = componentName;
                break;
            case "Fan #3":
                FAN2color.value = value/100;
                FAN2color.dataName = sensorName;
                FAN2color.componentName = componentName;
                break;
            case "Fan #5": // get the same value
                FAN3color.value = value/100;
                FAN3color.dataName = sensorName;
                FAN3color.componentName = componentName;
                FAN4color.value = value/100;
                FAN4color.dataName = sensorName;
                FAN4color.componentName = componentName;
                break;
        }
    }
}
