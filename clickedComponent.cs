using System.Collections;
using System.Collections.Generic;
using TMPro;
using Unity.VisualScripting;
using UnityEngine;

public class clickedComponent : MonoBehaviour
{
    public TMP_Text HUD_componentNameTXT;
    public TMP_Text HUD_sensorDataTXT;

    private void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit hitInfo;

            if (Physics.Raycast(ray, out hitInfo))
            {
                colorChanger component = hitInfo.transform.GetComponent<colorChanger>();
                if (component != null)
                {
                    string name = component.componentName;
                    if(name == "" || name == null)
                    {
                        HUD_componentNameTXT.text = component.gameObject.name; 
                    } else {
                        HUD_componentNameTXT.text = component.componentName;
                    }
                    HUD_sensorDataTXT.text =  component.dataName + ": " + component.value.ToString();
                }
            }
        }
    }
}
