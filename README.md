# Here are the scripts for the digital twin of my pc video
Video link: https://youtu.be/UcUdsMZZ2bY
- Unity version 2022.3.13f1
- .net version 7.0.203

If you want to run this on your computer here are the instuctions

```
Create a new .net project for HardwareMonitor.cs
Compile the HardwareMonitor.cs
https://learn.microsoft.com/en-us/dotnet/core/deploying/single-file/overview?tabs=cli

Place the created exe in unity in ExternalProcess in Assets folder

Other scripts in your unity Scripts folder

Assemble the 3dmodel and script as in the video
```
