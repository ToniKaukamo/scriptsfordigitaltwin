using UnityEngine;
using System.Diagnostics;
using System.Threading.Tasks;
using TMPro;

public class externalProcessHandler : MonoBehaviour
{
    public TMP_Text indicator;
    public GameObject fakeJson;
    public void Start()
    {
        #if UNITY_STANDALONE_WIN
        run_SensorData();
        this.indicator.text = "Real data from windows";
    }
    public void Update()
    {
        #else
        UnityEngine.Debug.Log("fakeGen");
        this.indicator.text = "Generated fake data";
        fakeJson.GetComponent<fakeJsonGenerator>().GenerateAndSaveFakeData();
        #endif
    }
    public async void run_SensorData()
    {
        // Update() but it actually waits for the exe to exit
        while (true)
        {
            Task task = new Task(realProcess);
            task.Start();
            UnityEngine.Debug.Log("Exe Running");
            await task;
        }
    }

    public void realProcess()
    {
        ProcessStartInfo pStartInfo = new ProcessStartInfo();
        pStartInfo.FileName = "Assets/ExternalProcess/SensorData.exe";
        pStartInfo.RedirectStandardOutput = true;
        pStartInfo.UseShellExecute = false;
        pStartInfo.CreateNoWindow = true;
        using (Process process = new Process())
        {
            process.StartInfo = pStartInfo;
            process.Start();
            process.WaitForExit();
            if(process.ExitCode != 0) {
                Application.Quit();
            }
        }
    }
}
